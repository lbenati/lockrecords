<?php namespace StudioBosco\LockRecords\Models;

use Model;
/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'studiobosco_lockrecords_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
