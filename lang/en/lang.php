<?php

return [
    'plugin' => [
        'name' => 'Lock Records',
        'description' => 'Allows to automatically lock opened records for other backend users.',
        'namespace' => '',
    ],
    'permissions' => [
        'auto_lock_records' => 'Lock records during edit for others.',
        'manually_unlock_records' => 'Manually unlock records.',
        'edit_settings' => 'Manage settings',
    ],
    'messages' => [
        'unlock_record' => 'Unlock',
        'unlock_record_confirm' => 'Really unlock this record?',
        'unlock_record_success' => 'Record unlocked.',
        'locked_at' => 'locked at',
        'locked_by' => 'locked by :editor_name',
        'record_locked_when_updating' => 'Record :record_name with ID :record_id is locked by :editor_name and cannot be saved.'
    ],
    'settings' => [
        'description' => 'Settings for locking records.',
        'max_age' => 'Amount of minutes before unlocking automatically.',
    ],
];
