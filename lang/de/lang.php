<?php

return [
    'plugin' => [
        'name' => 'Datensätze sperren',
        'description' => 'Erlaubt das automatische Sperren von offenen Datensätzen für andere Backend-Nutzer*innen.',
        'namespace' => '',
    ],
    'permissions' => [
        'auto_lock_records' => 'Datensätze bei Bearbeitung für andere sperren.',
        'manually_unlock_records' => 'Datensätze manuell entsperren.',
        'edit_settings' => 'Einstellungen verwalten',
    ],
    'messages' => [
        'unlock_record' => 'Entsperren',
        'unlock_record_confirm' => 'Diesen Datensatz wirklich entsperren?',
        'unlock_record_success' => 'Datensatz entsperrt.',
        'locked_at' => 'gesperrt am',
        'locked_by' => 'gesperrt durch :editor_name',
        'record_locked_when_updating' => 'Datensatz :record_name mit der ID :record_id ist durch :editor_name gesperrt und kann nicht gespeichert werden.',
    ],
    'settings' => [
        'description' => 'Einstellung zum sperren von Datensätzen.',
        'max_age' => 'Anzahl Minuten bis zum automatischen entsperren.',
    ],
    'models' => [
        'general' => [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],
        'pendingchange' => [
            'label' => 'Pending Change',
            'label_plural' => 'Pending Changes',
        ],
    ],
];
