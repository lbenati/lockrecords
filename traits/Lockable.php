<?php namespace StudioBosco\LockRecords\Traits;

use App;
use Log;
use Flash;
use BackendAuth;
use Carbon\Carbon;
use StudioBosco\LockRecords\Models\LockedRecord;
use StudioBosco\LockRecords\Models\PendingChange;
use StudioBosco\LockRecords\Models\Settings;

trait Lockable
{
    public function isLocked($user = null)
    {
        // locks do not work when working from the CLI
        if (App::runningInConsole()) {
            #return false;
        }

        $user = $user ?: BackendAuth::getUser();
        $lock = $this->getLock();

        if ($lock) {
            $now = Carbon::now();
            $maxAge = intval(Settings::get('max_age', 10));
            $touchedAt = ($lock->updated_at ?: $lock->created_at)->clone();

            // check if lock is older then 10 minutes after last update
            // if so the lock is no longer valid
            if ($touchedAt->addMinutes($maxAge)->lessThan($now)) {
                $lock->delete();
                return false;
            }

            if (!$user) {
                return true;
            } elseif ($user->id != $lock->editor_id) {
                return true;
            }
        }

        return false;
    }

    public function scopeIsLocked($query, $user = null)
    {
        $user = $user ?: BackendAuth::getUser();

        if ($user) {
            return $query->whereHas('lock', function ($query) use ($user) {
                $query->whereNot('editor_id', $user->id);
            });
        } else {
            return $query->whereHas('lock');
        }
    }

    public function scopeIsUnlocked($query, $user = null)
    {
        $user = $user ?? BackendAuth::getUser();

        if ($user) {
            return $query->whereHasNot('lock', function ($query) use ($user) {
                $query->whereNot('editor_id', $user->id);
            });
        } else {
            return $query->whereHasNot('lock');
        }
    }

    public function unlock()
    {
        $lock = $this->getLock();

        if ($lock) {
            $lock->delete();
            $this->applyPendingChanges();
        }
    }

    public function lock($user = null)
    {
        $user = $user ?: BackendAuth::getUser();

        if (!$user) {
            return;
        }

        if ($this->isLocked($user)) {
            return;
        }

        $lock = $this->recordLock ?: new LockedRecord();
        $lock->record = $this;
        $lock->editor = $user;
        $lock->touch();
        $lock->save();
    }

    public function getLock()
    {
        return $this->recordLock;
    }

    public function queueChanges()
    {
        $changes = $this->getDirty();

        $this->pendingChanges()->create([
            'changed_attributes' => $changes,
        ]);
    }

    public function applyPendingChanges()
    {
        if (!$this->pendingChanges()->count()) {
            return;
        }

        Log::info('Applying pending changes of previously locked ' . $this::class . ' with ID ' . $this->id . '.');

        foreach($this->pendingChanges()->get() as $record) {
            $changes = $record->changed_attributes;

            foreach($changes as $key => $value) {
                $this->$key = $value;
            }
        }

        $this->pendingChanges()->delete();
        $this->save();
    }

    /**
     * Boot the lockable trait for a model.
     * @return void
     */
    public static function bootLockable()
    {
        // attach relationship
        static::extend(function ($model) {
            $model->morphOne['recordLock'] = [
                LockedRecord::class,
                'name' => 'record',
            ];
            $model->morphMany['pendingChanges'] = [
                PendingChange::class,
                'name' => 'record',
                'order' => 'created_at asc',
            ];
        });

        // prevent model from updating if it is locked
        static::updating(function ($model) {
            if ($model->isLocked()) {

                Log::warning('Record of type ' . $model::class . ' with ID ' . $model->id . ' is locked by user ' . $model->getLock()->editor->login . '. Changes will be applied once unlocked.');
                $model->queueChanges();

                $user = BackendAuth::getUser();

                if ($user) {
                    Flash::warning(trans('studiobosco.lockrecords::plugin.messages.record_locked_when_updating', ['record_name' => $model::class, 'record_id' => $model->id, 'editor_name' => $model->getLock()->editor->login]));
                }
                return false;
            }
        });

        // delete pending changes when the model get's deleted
        static::deleting(function ($model) {
            $model->pendingChanges()->delete();
        });
    }
}
