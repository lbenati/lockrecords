<?php if ($formModel->isLocked()) : ?>
    <button
        type="button"
        data-request="onUnlockRecord"
        data-request-confirm="<?= e(trans('studiobosco.lockrecords::lang.messages.unlock_record_confirm')); ?>"
        data-request-data="redirect_to: 'update'"
        class="btn btn-danger"
    >
        🔐 <?= e(trans('studiobosco.lockrecords::lang.messages.unlock_record')); ?>
    </button>
<?php endif; ?>
