window.addEventListener('beforeunload', function (event) {
    delete event['returnValue'];
    const form = document.getElementById('Form');
    $(form).request('onUnlockRecord');
}, false);

function lockRecord() {
    const url = window.location.href;
    const tokenInput = document.querySelector('input[name="_token"]');
    const sessionKeyInput = document.querySelector('input[name="_session_key"]');

    if (!tokenInput) {
        return;
    }

    fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'X-CSRF-TOKEN': tokenInput.value,
            'X-WINTER-REQUEST-HANDLER': 'onLockRecord',
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            _token: tokenInput.value,
            _session_key: sessionKeyInput ? sessionKeyInput.value : null,
        }),
    });
}

lockRecord();

window.setInterval(lockRecord, 5000);
